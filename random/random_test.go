package random

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"

	"fmt"
)

type sequenced struct {
	Count int
	Total int
}

func (seq *sequenced) Seek(n int) error {

	if n < 0 {
		return fmt.Errorf("invalid seek position %d < 0",n)
	}

	if n >= seq.Total {
		return fmt.Errorf("invalid seek position %d >= %d",n,seq.Total)
	}

	seq.Count = n

	return nil
}

func (seq *sequenced) Read(n []byte) (int,error) {

	for i := 0; i < len(n); i++ {
		n[i] = byte(seq.Count)
		seq.Count++		
	}
	return len(n),nil
}

func (seq *sequenced) Pos() int {

	return seq.Count
}

func (seq *sequenced) Len() int {

	return seq.Total
}


func Test_Basic(t *testing.T) {

	var basic *Basic

	Convey("New",t,func() {

		b,err := New(&sequenced{Count:0,Total:1000})
		So(err,ShouldBeNil)
		So(b,ShouldNotBeNil)
		So(b.internal,ShouldNotBeNil)

		basic = b
	})

	Convey("Raw Normals",t,func() {

		n,err := basic.Raw()
		So(err,ShouldBeNil)


		max := 0
		
		for i := 0; i < len(n); i++ {
			if n[i] > max {
				max = n[i]
			}
		}

		fmt.Printf("max = %d\n",max)
			
	})


	Convey("Normals",t,func() {

		for i := 0; i < 100; i++ {
			n,err := basic.Normal()
			So(err,ShouldBeNil)
		
			fmt.Printf("%03d normal %f\n",i,n)
		}
	})

	Convey("Uniforms",t,func() {
			
		for i := 0; i < 100; i++ {
			u,err := basic.Uniform()
			So(err,ShouldBeNil)

			fmt.Printf("%03d uniform %f\n",i,u)
		}
	})

}
