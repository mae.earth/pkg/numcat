package random

import (
	"mae.earth/pkg/numcat"
)

const __size int = 158757976233090
const np float64 = 1.0 / float64(__size)

type Basic struct {
	internal *numcat.CatData
}

func (b *Basic) Raw() ([]int,error) {

	r := make([]int,1000)

	b.internal.Lock()
	defer b.internal.Unlock()

	n := b.internal.Read(r)

	if b.internal.Increment(n) >= 100000 {
		if err := b.internal.Next(true); err != nil {
			return nil,err
		}
	}

	return r,nil
}

/* [0.0,1.0] */
func (b *Basic) Uniform() (float64,error) {

	r := make([]int,1)

	b.internal.Lock()
	defer b.internal.Unlock()

	n := b.internal.Read(r)

	if b.internal.Increment(n) >= 100000 {
		if err := b.internal.Next(true); err != nil {
			return 0.5,err
		}
	}

	return (np * float64(r[0])),nil
}


/* [-1.0,1.0] */
func (b *Basic) Normal() (float64,error) {

	r := make([]int,2)

	b.internal.Lock()
	defer b.internal.Unlock()

	n := b.internal.Read(r)

	if b.internal.Increment(n) >= 100000 {
		if err := b.internal.Next(true); err != nil {
			return 0.0,err
		}
	}

	p := (np * float64(r[0])) - (np * float64(r[1]))

	return p,nil 
}



func New(cat numcat.Cataloguer) (*Basic,error) {

	if cat == nil {
		return nil,numcat.ErrInvalidCatalogue
	}

	d,err := numcat.NewCatData(cat)
	if err != nil {
		return nil,err
	}

	if err := d.Next(false); err != nil {
		return nil,err
	}

	return &Basic{d},nil
}
