package numcat

import (
	"fmt"
	"sync"
)


var (
	ErrCatalogueExhasted error = fmt.Errorf("catalogue exhasted")
	ErrInvalidCandiates error = fmt.Errorf("invalid candiate count")
	ErrInvalidCatalogue error = fmt.Errorf("invalid catalogue")
	ErrMinGreaterThanMax error = fmt.Errorf("min >= max")
	ErrMinLessThanZero error = fmt.Errorf("min < 0")
	ErrInvalidNumberPer error = fmt.Errorf("invalid number per value, must be > 0")
	ErrMustBeGreaterThanZero error = fmt.Errorf("must be greater than 0")
)


type Cataloguer interface {
	Seek(int) error
	Read([]byte) (int,error)
	Pos() int
	Len() int
}


type CatData struct {
	sync.Mutex
	z,w,counter,position int
	catalogue Cataloguer
}

func (d *CatData) Increment(n int) int {
	d.counter += n
	return d.counter
}
	

func (d *CatData) Next(roll bool) error {

	in := make([]byte,4)
	n,err := d.catalogue.Read(in)
	if err != nil {
		return err
	}
	if n != 4 {
		return ErrCatalogueExhasted
	}
	
	y := int(in[0])
	if y == 0 {
		y = 1
	}
	z := int(in[1])
	if z == 0 {
		z = 1
	}

	d.z = y * z

	x := int(in[2])
	if x == 0 {
		x = 1
	}

	w := int(in[3])
	if w == 0 {
		w = 1
	}

	d.w = x * w

	d.position += 4

	if d.position >= d.catalogue.Len() {
	
		if roll {
			d.catalogue.Seek(0)
		} else {
			return ErrCatalogueExhasted
		}
	}
	d.counter = 0	
	return nil
}

func (d *CatData) Read(list []int) int {

	for i := 0; i < len(list); i++ {
		d.z = 36969 * (d.z & 65535) + (d.z >> 16)
		d.w = 18000 * (d.w & 65535) + (d.w >> 16)
		list[i] = (d.z << 16) + d.w
	}
	return len(list)
}

func NewCatData(cat Cataloguer) (*CatData,error) {

	if cat == nil {
		return nil,ErrInvalidCatalogue
	}

	d := &CatData{catalogue:cat}	
	if err := d.Next(false); err != nil {
		return nil,err
	}

	return d,nil
}

	






