package montecarlo

import (
	
	"mae.earth/pkg/numcat"
)

/* TODO: add an test mode where the catalogue is exhausted and the distribution presented */

const __size int = 100000000000000//158757976233090 
const np float64 = 1.0 / float64(__size)

const DefaultRollForwards int = 10000000

type Configuration struct {

	Roll bool /* roll catalogue around when hit the end ? */
	RollForwards int
	NumberPer int
}

type Montecarlo struct {
	internal *numcat.CatData
	
	Config *Configuration
}



func (m *Montecarlo) Linear(min,max int) (int,error) {
		
	if min >= max {
		return 0,numcat.ErrMinGreaterThanMax
	}

	if min < 0 {
		return 0,numcat.ErrMinLessThanZero
	}


	r := make([]int,m.Config.NumberPer) 
	
	m.internal.Lock()
	defer m.internal.Unlock()

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return 0,err
		}
	}

	return Linear(r,min,max),nil
}

func (m *Montecarlo) Linearv(v,min,max int) ([]int,error) {

		if min >= max {
			return nil,numcat.ErrMinGreaterThanMax
		}
		if min < 0 {
			return nil,numcat.ErrMinLessThanZero
		}

		m.internal.Lock()
		defer m.internal.Unlock()
		
		out := make([]int,0)
		for i := 0; i < v; i++ {
				r := make([]int,m.Config.NumberPer)
				n := m.internal.Read(r)

				if m.internal.Increment(n) >= m.Config.RollForwards {
					if err := m.internal.Next(m.Config.Roll); err != nil {
						return nil,err
					}
				}
		
				out = append(out,Linear(r,min,max))
		}

		return out,nil
}				

func (m *Montecarlo) RedBlack() (float64,error) {
	
	r := make([]int,m.Config.NumberPer)
	
	m.internal.Lock()
	defer m.internal.Unlock()
	
	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards { 
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return 0.0,err
		}
	}

	return RedBlack(r,len(r),len(r) / 2),nil
}

func (m *Montecarlo) WhiteBlack() (float64,error) {

	r := make([]int,m.Config.NumberPer) 

	m.internal.Lock()
	defer m.internal.Unlock()

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
			if err := m.internal.Next(m.Config.Roll); err != nil {
				return 0.0,err
			}
	}

	return WhiteBlack(r,len(r),len(r) / 2),nil
}

/* [-1,1.0] */
func (m *Montecarlo) Normal() (float64,error) {

	r := make([]int,m.Config.NumberPer)

	m.internal.Lock()
	defer m.internal.Unlock()

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return 0.0,err
		}
	}

	a := 0
	for _,r1 := range r {
		a += r1
	}

	a /= len(r)

	n = m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return 0.0,err
		}
	}

	b := 0
	for _,r1 := range r {
		b += r1
	}

	b /= len(r) 


	p := (np * float64(a)) - (np * float64(b))

	
	return p,nil
}

func (m *Montecarlo) Float64() (float64,error) {

	r := make([]int,m.Config.NumberPer)

	m.internal.Lock()
	defer m.internal.Unlock()

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return 0.0,err
		}
	}

	a := 0.0
	for _,r1 := range r {
		a += float64(r1)
	}

	a /= float64(len(r))

 	return (np * a),nil
}

func (m *Montecarlo) Float64V(d int) ([]float64,error) {

	if d <= 0 {
		return nil,numcat.ErrMustBeGreaterThanZero
	}

	r := make([]int,m.Config.NumberPer)

	m.internal.Lock()
	defer m.internal.Unlock()

	out := make([]float64,d)

	for i := 0; i < d; i++ {
		n := m.internal.Read(r)

		if m.internal.Increment(n) >= m.Config.RollForwards {
			if err := m.internal.Next(m.Config.Roll); err != nil {
				return nil,err
			}
		}

		a := 0.0
		for _,r1 := range r {
			a += float64(r1)
		}
		a /= float64(len(r))

		out[i] = (np * a)
	}

	return out,nil
}





func (m *Montecarlo) Election(candiates int) ([]int,error) {

	if candiates <= 0 {
		return nil,numcat.ErrInvalidCandiates
	}
	
	m.internal.Lock()
	defer m.internal.Unlock()

	r := make([]int,(candiates * m.Config.NumberPer)) 
	
	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return nil,err
		}
	}

	return Election(candiates,r),nil
}

func (m *Montecarlo) ElectionString(candiates []string) ([]string,error) {
	
	if len(candiates) <= 0 {
		return nil,numcat.ErrInvalidCandiates
	}

	m.internal.Lock()
	defer m.internal.Unlock()

	r := make([]int,(len(candiates) * m.Config.NumberPer))

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return nil,err
		}
	}

	return ElectionString(candiates,r),nil
}


func (m *Montecarlo) ElectionList(list []int) ([]int,error) {

	if len(list) == 0 {
		return nil,numcat.ErrInvalidCandiates
	}

	m.internal.Lock()
	defer m.internal.Unlock()

	r := make([]int,(len(list) * m.Config.NumberPer))

	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return nil,err
		}
	}


	results := Election(len(list),r)
	out := make([]int,0)

	for i := 0; i < len(results); i++ {		
			out = append(out,list[results[i] - 1])
	}

	return out,nil
}
			

func (m *Montecarlo) Elimination(candiates,rounds int)([]int,error) {

	if candiates <= 0 {
		return nil,numcat.ErrInvalidCandiates
	}

	m.internal.Lock()
	defer m.internal.Unlock()

	r := make([]int,(candiates * m.Config.NumberPer)) 
	
	n := m.internal.Read(r)

	if m.internal.Increment(n) >= m.Config.RollForwards {
		if err := m.internal.Next(m.Config.Roll); err != nil {
			return nil,err
		}
	}

	return Elimination(candiates,rounds,r),nil
}

func New(config *Configuration,cat numcat.Cataloguer) (*Montecarlo,error) {

	if cat == nil {
		return nil,numcat.ErrInvalidCatalogue
	}

	if config == nil {
		config = &Configuration{Roll:true,RollForwards:DefaultRollForwards,NumberPer:99}
	}

	if config.NumberPer <= 0 {
		return nil,numcat.ErrInvalidNumberPer
	}

	d,err := numcat.NewCatData(cat)
	if err != nil {
		return nil,err
	}

	if err := d.Next(false); err != nil {
		return nil,err
	}
	

	m := &Montecarlo{}
	m.internal = d

	m.Config = config
	
	return m,nil
}

	


